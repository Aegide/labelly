extends Node

var DB_status = true

var DB_characters = {}
var DB_requests = {}


var character_folder = "res://assets/chararacters/"
var list_available_character_ID = []

var characters_today = []

var player_alignement = 50
var player_reputation = 50

var characters_filename = "res://databaseCSV/Persos LAB Elly - Characters.csv"
var requests_filename = "res://databaseCSV/Persos LAB Elly - Requests.csv"


func _ready():
	
#	DB_status = load_DB(characters_filename,DB_characters)
#	print("\nDB Personnages est bien formatée ? : ",DB_status,"\n")
#
#	DB_status = load_DB(requests_filename,DB_requests)
#	print("\nDB Requêtes est bien formatée ? : ",DB_status)
#
	
	
	
	
	
	#PERSONNAGE
#	if (false):
		
		#Démonstration de la DB
#		init_character_randomizer(DB_characters,list_available_character_ID)
#		print(list_available_character_ID)
#		var character = get_random_character(DB_characters,list_available_character_ID)
#		print(character)
#		display_character(character)
		
		#Démonstration de choix d'un perso "disponible"
#		update_available_characters(DB_characters,list_available_character_ID)
#		print(list_available_character_ID)
#		var character2 = get_random_character(DB_characters,list_available_character_ID)#TODO
#		print(character2)
#		display_character(character2)
		
		#Démonstration d'une journée (5 personnages parmi ceux disponibles)
#		update_available_characters(DB_characters,list_available_character_ID)
#		characters_today = get_random_character_combination(DB_characters,list_available_character_ID,5)
		#print(characters_today)
	
	
	#REQUETE
#	if (false):
		
		#test 1 osef
#		var request
#		var requestConsequence
#		for key_request in DB_requests:
#			request=DB_requests[key_request]
			#requestConsequence = get_consequence(request,1)
			#analyse_request_consequence(requestConsequence)
		
		#test 2 : récupérer une requête parmi plusieurs pour un personnages
#		update_available_characters(DB_characters,list_available_character_ID)
#		var character = get_random_character(DB_characters,list_available_character_ID)
#		print("[[ ",get_name(character)," ]]\n")
#
#		#Listes de clé de requêtes selon "CharacterID" et "Unlocked"
#		var request_list = get_request_list(DB_requests,character)
#		var request2 = get_random_request(DB_requests,request_list)
#		print(get_request_text(request2))
	pass


#FONCTIONS DE REQUETE

func get_random_request(request_database,list_request_ID):

	if (list_request_ID.size()==0):
		return null
	else:
			
		var random_key = randi()%list_request_ID.size()
		var key_chosen = list_request_ID[random_key]
		return request_database[key_chosen]

func get_request_list(request_database,character):
	#print("character : ",character)
	var character_ID = get_id(character)
	var request_list = []
	var request
	var request_char_ID
	
	for request_key in request_database:
		request = request_database[request_key]
		request_char_ID = get_character_id(request)
		
		if(request_char_ID==character_ID and is_request_available(request)):
			request_list.append(request_key)		
#			print("[Accepté] : ",get_request_text(request),"\n")
#			print(request_char_ID==character_ID)
#			print(is_request_available(request))
#			print(request["Unlocked"])
#			print(" ")
#		else:
#			print("[Refusé] : ",get_request_text(request),"\n")

	return request_list

func get_value_consequence(consequence):
	
	var tmp = consequence.split("(")# "Gold(5)" => "Gold" + "5)"  
	var tmp2 = (tmp[1]).split(")")# "5)" => "5" + ""   
	return tmp2[0]
	
func get_request_text(request):
	return request["Request"]

func get_answerCount(request):
	var answerCount = 0
	for i in range(1,5):
		if (get_answer(request,i)!=""):
			answerCount+=1
	return answerCount

func get_answer(request,number):
	if(number<1 or number>4):#1,2,3,4
		return null
	var x = "Answer"+ str(number)
	return request[x]

func get_character_id(request):
	return request["CharacterID"]

func is_request_available(request):
	return is_unlocked(request)

func get_consequence(request,number):
	if(number<1 or number>4):#1,2,3,4
		return null
	var x = "Consequence"+ str(number)
	return request[x]




#FONCTIONS DE PERSONNAGE

#fonction de démonstration
func display_character(character):
	
	#UNLOCKED
	var colorNode = get_node("ColorRect")
	if(is_unlocked(character)):
		colorNode.set_frame_color(Color(0, 0.75, 0))
	else:
		colorNode.set_frame_color(Color(0.75, 0, 0))
	
	#TEXTURE
	var textureNode = get_node("ColorRect/VBoxContainer/Sprite")
	var pathToTexture = character_folder + get_texture_name(character) + ".png"
	var texture = load(pathToTexture)
	textureNode.set_texture(texture)
	
	#NOM
	var nameNode = get_node("ColorRect/VBoxContainer/Name")
	nameNode.set_text(get_name(character))
	
	
	pass

#fonction de démonstration
func init_character_randomizer(character_database,list_character_ID):
	randomize()
	for i in character_database:
		list_character_ID.append(get_id(character_database[i]))
	pass


#genère une liste de personnage de taile "amount"  => une journée
func get_random_character_combination(character_database,list_character_ID,amount):
	
	
	
	var characters_today = []
	var character_key
	var character
	var list_size = list_character_ID.size()
	
	#print("list_character_ID : ",list_character_ID)
	
	#cas exceptionnel : il y au plus, autant de persos demandés que de persos possibles
	if(list_size<=amount):
		print(">>liste peu remplie")
		return null
		
	else:
		#print(list_character_ID)
		while(amount>0):
			character_key = get_random_character_key(character_database,list_character_ID)
			character = character_database[character_key]
			characters_today.append(character)
			list_character_ID.erase(character_key)
			
			amount-=1
#			print(">>ajout d'un perso",character,"\n")

	
	return characters_today

func get_random_character(database,list_character_ID):
	
	var random_key = randi()%list_character_ID.size()
	var key_chosen = list_character_ID[random_key]
	return database[key_chosen]

func get_random_character_key(database,list_character_ID):

	var random_key = randi()%list_character_ID.size()
	return list_character_ID[random_key]

func update_available_characters(character_database,list_character_ID):
	
	randomize()
	list_character_ID.clear()
	
	var character
	for key_character in character_database:
		#print(character_database[key_character],key_character,"\n")
		character = character_database[key_character]
		
		if is_character_available(character):
			list_character_ID.append( get_id(character) )
#			print( "[",get_name(character),"] est disponible" )	
#		else:
#			print( "<",get_name(character),"> est indisponible" )	
		
	pass 

func is_character_available(character):
	
	if not is_unlocked(character):
		return false
	
	elif get_minimum_reputation(character) > player_reputation:
		return false
	
	elif get_maximum_reputation(character) < player_reputation:
		return false
	
	elif get_minimum_alignement(character) > player_alignement:
		return false
	
	elif get_maximum_alignement(character) < player_alignement:
		return false

	return true

func get_texture_name(character):
	return character["TextureName"]

func get_image(character):
	return load(character_folder + get_texture_name(character) + ".png")

func get_name(character):
	return character["Name"]

func get_minimum_reputation(character):
	return int(character["MinimumReputation"])

func get_maximum_reputation(character):
	return int(character["MaximumReputation"])

func get_minimum_alignement(character):
	return int(character["MinimumAlignement"])

func get_maximum_alignement(character):
	return int(character["MaximumAlignement"])

func get_color(character):
	return (character["Color"])

	
	
	

#REQUETE ET PERSONNAGES
func set_unlocked(element, is_active):
	element["Unlocked"]=is_active
	pass

func is_unlocked(element):
	return element["Unlocked"]=="TRUE"

func get_id(element):
	return element["ID"]

func toggle_element(database, element_id, is_active):
	var tmp
	for element_key in database:
		tmp = database[element_key]
		if( get_id(tmp) == str(element_id) ):
			set_unlocked(tmp,is_active)
	pass


#FONCTIONS DE DB

func line_have_incorrect_size(line,attributes_amount):
	return not line.size()==attributes_amount

func load_DB(csv_file,database):
	
	
	var file = File.new()
	
	if not file.file_exists(csv_file):
#
#		print(">>Absence de DB")
		return null
#	else:
#		print(">>BD présente")
	
	file.open(csv_file,file.READ)
	
	
	
	
	
	
	var pos = 0
	var file_len = file.get_as_text().length()
	var is_header = true
	var attributes_list = []
	var attributes_amount = 0
	var temp_dict = {}
	var verif_id_list = []
	
	file.seek(0)
	var successfully_read = true

	while !file.eof_reached():
		
		var line = file.get_csv_line()
		#print(line)
		
		if is_header == true:
			attributes_list = line
			attributes_amount = line.size()
			is_header = false
			
		else:
			#Vérifie que le nombre de composants est correct
			if(line_have_incorrect_size(line,attributes_amount)):
				successfully_read=false
				print(">> line_have_incorrect_size")
			
			#print(line[0])
			
			#Vérifie que les ID sont uniques
			if(line[0] in verif_id_list):
				successfully_read=false
				print(">> non_unique_ID")
			else:
				verif_id_list.append(line[0])
			
			
			
			#Ajoute l'élément à la DB
			for i in range(0,attributes_amount):
				temp_dict[attributes_list[i]] = line[i]
				
#
#				if(csv_file==requests_filename and 7 <= i and i <= 10):
#					print(line[i])
#					if(line[i]==""):
#						print("VIDE")
#
#			if(csv_file==requests_filename):
#				print(" ")
				
			database[line[0]] = temp_dict
			temp_dict = {}	
				
	return successfully_read

