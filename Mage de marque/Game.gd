extends Control

# Interface
var AlignementBar
var PopularityBar
var GoldCounter

# Base de données
var DB
var DB_status = false
var DB_characters = {}
var DB_requests = {}


# Élements pour la sélection aléatoire
var list_available_character_ID = []
var request_list = []
var request


# Éléments de la journée
var characters_today = []
var requests_today = []
var option_list_order = []

var daily_tax = 15
var allowed_debt = -100
var starting_gold = 100
var first_turn = true

# Éléments de l'interface
var CharacterNode
var DialogueNode

var ChoiceOneNode
var ChoiceTwoNode
var ChoiceThreeNode
var ChoiceFourNode

var ChoiceOneNodeText
var ChoiceTwoNodeText
var ChoiceThreeNodeText
var ChoiceFourNodeText

var AudioStreamNode
var DayCounterNode

var BT

#trucs
var DialogueList=[]
var character_image
var request_tmp
var character_tmp
var is_loading_data = false
var playback_position
var day_count


func _ready():
	
	DB = load("res://databaseCSV/databaseCSV.gd").new()
	
	#Signaux des options
	init_signals()
	
	#Connecte les nodes à leur variables respectives
	init_interface()
	

	
	if(GlobalScript.isNewGame):
		# Bases de données
		init_databases()
	
		#Préparation d'un jour (5 perso => 5 requêtes)
		prepare_today()
		
	else:	
		#Chargement de la sauvegarde
		load_game_data()
	
	#Prends la première requête de la liste, et la charge à l'écran
	prepare_current_request()
	
	pass


func game_over():
	print("avant")
	get_tree().change_scene("res://GameOverMenu.tscn")
	print("apres")
	pass

func prepare_today():
	
	
	#TAXES
	if(first_turn):
		# Pas de taxes/dettes au premier tour
		first_turn = false
		day_count = 1
		print("[[ Première journée ]]")
		
	else:
		# Taxes 
		var gold = int(GoldCounter.get_text()) - daily_tax
		GoldCounter.set_text(String(gold))
		day_count += 1
		print("[[ Nouvelle journée ]]")
	
	
	
	DayCounterNode.text = "Jour : " + str(day_count)
	
#		#DETTES
#		if( gold < allowed_debt ):
#			# Trop de dettes, fin du jeu
#			print("[[ Game over]]")
#			game_over()
#		else:
#			# Nouvelle journée, sans trop de dettes
#			print("[[ Nouvelle journée ]]")
		
	
	
	DB.update_available_characters(DB_characters,list_available_character_ID)
	
	
	characters_today = DB.get_random_character_combination(DB_characters,[]+list_available_character_ID,5)
	
	var tmp = (characters_today==null)
	
	if(tmp):
		print("game over")
		GlobalScript.gameOverText="Perdu trop de clients"
		game_over()

		
		
	#print(list_available_character_ID.size() , " <=> ",characters_today.size())
	
	#TODO
#	if(list_available_character_ID.size() < 5):
	
	
	
	
	
	
	requests_today.clear()
	for character in characters_today:
		request_list = DB.get_request_list(DB_requests,character)
		request = DB.get_random_request(DB_requests,request_list)
		requests_today.append(request)
		#print(DB.get_id(character)," => ",DB.get_id(request))
	pass

#Requête courante = la première requête dans la liste des requêtes du jour
func prepare_current_request():
	
	#print("BBBBBBB")
	
	
	prepare_character_sprite()
	
	prepare_character_dialogue()
	
	prepare_request_choices()
	
	pass

#Fonction à appeler 1 fois pour initialiser l'interface
func init_interface():
	var tmpNode
	
	AlignementBar = _getAlignementBar()
	AlignementBar.set_as_ratio( 0.5 )
	
	PopularityBar = _getPopularityBar()
	PopularityBar.set_as_ratio( 0.25 )
	
	GoldCounter = _getGoldCounter()
	GoldCounter.set_text(""+str(starting_gold))
	
	tmpNode = get_node("GUI/GUI_list/UpperUI/ZoneCharacter/Character")
	CharacterNode = tmpNode.get_node("TextureRect")
	
	tmpNode = get_node("GUI/GUI_list/UpperUI/ZoneDialogue/DialogueBox")
	DialogueNode = tmpNode.get_node("MarginContainer/TextEdit")
	
	AudioStreamNode = get_node("AudioStreamPlayer")
	DayCounterNode = get_node("GUI/GUI_list/UpperUI/ZoneCharacter/DayCounter")
	
	var max_visible_characters = 100
	
	ChoiceOneNode = get_node("GUI/GUI_list/DialogueChoices/ListeChoices/Choice1")
	ChoiceOneNodeText = ChoiceOneNode.get_node("ChoiceContainer/RichTextLabel")
	ChoiceOneNodeText.set_visible_characters(max_visible_characters)
	
	ChoiceTwoNode = get_node("GUI/GUI_list/DialogueChoices/ListeChoices/Choice2")
	ChoiceTwoNodeText = ChoiceTwoNode.get_node("ChoiceContainer/RichTextLabel")
	ChoiceTwoNodeText.set_visible_characters(max_visible_characters)
	
	ChoiceThreeNode = get_node("GUI/GUI_list/DialogueChoices/ListeChoices/Choice3")
	ChoiceThreeNodeText = ChoiceThreeNode.get_node("ChoiceContainer/RichTextLabel")
	ChoiceThreeNodeText.set_visible_characters(max_visible_characters)
	
	ChoiceFourNode = get_node("GUI/GUI_list/DialogueChoices/ListeChoices/Choice4")
	ChoiceFourNodeText = ChoiceFourNode.get_node("ChoiceContainer/RichTextLabel")
	ChoiceFourNodeText.set_visible_characters(max_visible_characters)
	
#	ChoiceFourNode.set_
#	BT = load("res://Choice.gd").new()
#	BT._on_TextureButton_pressed()

	
	
	pass

#Fonction à appeler 1 fois pour initialiser les base de données
func init_databases():
	

	#DB_status = DB.load_DB("res://databaseCSV/Persos LAB Elly - Characters.json",DB_characters)
	
	DB_status = DB.load_DB("res://databaseCSV/Persos LAB Elly - Characters.csv",DB_characters)
	print("\nDB Personnages est bien formatée ? : ",DB_status)
	#exit_game_when_false(DB_status)
	
	DB_status = DB.load_DB("res://databaseCSV/Persos LAB Elly - Requests.csv",DB_requests)
	print("DB Requêtes est bien formatée ? : ",DB_status,"")
	#exit_game_when_false(DB_status)
	
	DB_status = test_db_requests(DB_requests)
	print("Requêtes sont correctes ? : ",DB_status,"\n")
	#exit_game_when_false(DB_status)
	pass


func test_db_requests(DB_requests):
	
	var is_correct = true
	
	for rq in DB_requests:
		
		request_tmp = DB_requests[rq]
#		print(request_tmp)
#		print(">>>>>")
		
		for i in request_tmp:
			if(i=="Consequence1" or i=="Consequence2"  or i=="Consequence3" or i=="Consequence4" ):
				if(request_tmp[i]!=""):
					#print(request_tmp[i])
					var rep = execute_request_consequence(request_tmp,request_tmp[i],false)
					if(rep!=null):
						is_correct = false
						print("ERREUR : ",rep)
						print("Personnage : ",request_tmp["CharacterID"])
						print("Requete : ",request_tmp["Request"])
#				else:
					#print("VIDE")
#		print("-----")
	
	
	return is_correct


#Fonction à appeler 1 fois pour initialiser les signaux
func init_signals():
	$GUI/GUI_list/DialogueChoices/ListeChoices/Choice1.connect("button_press", self, "choice_selected")
	$GUI/GUI_list/DialogueChoices/ListeChoices/Choice2.connect("button_press", self, "choice_selected")
	$GUI/GUI_list/DialogueChoices/ListeChoices/Choice3.connect("button_press", self, "choice_selected")
	$GUI/GUI_list/DialogueChoices/ListeChoices/Choice4.connect("button_press", self, "choice_selected")
	
	pass

#TODO
func choice_selected(choice_name):
	var choice_number = int(choice_name.substr(6,1))
	#print(">> Choix_A ",choice_number)
	
	
	choice_number = option_list_order[choice_number-1]
	character_tmp = characters_today.front()
	print(">> (",DB.get_name(character_tmp),") Choix ",choice_number)
	
	
	#Personnage/Requête actuelle
	request_tmp = requests_today.front()
	var consequence = DB.get_consequence(request_tmp,choice_number)
	execute_request_consequence(request_tmp,consequence,true)

	#Transition
	characters_today.pop_front()
	requests_today.pop_front()
	var remaining = characters_today.size()
	print("<< Personnage suivant (reste : ",remaining,")")
	
	#TODO
	
	if( remaining==0 or characters_today.size()==0 ):
		#Journée terminée => journée suivante
		print("[[ Journée terminée]]\n")
		prepare_today()
	else:
		print(" ")
	
	
	#print("AAAAAAAA")
	
	#Personnage suivant
	prepare_character_sprite()
	prepare_character_dialogue()
	prepare_request_choices()
	
	
	pass



func execute_request_consequence(current_request,request_consequence,if_execute):
	
	var consequence_list = request_consequence.split(";")
	var value
	var tmp
#	print(consequence_list)
	
	for consequence in consequence_list:
		
		if "Gold" in consequence:
			if(if_execute):
				value = int(DB.get_value_consequence(consequence))
				tmp = int(GoldCounter.get_text()) + value
				GoldCounter.set_text(  String(tmp)    )
				print("Or modifié : ",value)
			
			
		elif "Reputation" in consequence:
			if(if_execute):
				value = int(DB.get_value_consequence(consequence))
				tmp = int(PopularityBar.get_value())
				PopularityBar.set_value(tmp + value)
				print("Popularité modifiée : ",value,"(",PopularityBar.get_value(),")")
			
		elif "Alignement" in consequence:
			if(if_execute):
				value = int(DB.get_value_consequence(consequence))
				tmp = int(AlignementBar.get_value())
				AlignementBar.set_value(tmp + value)
				print("Alignement modifié : ",value,"(",AlignementBar.get_value(),")")
		
		
		#REQUÊTES
		elif "UnlockRequest" in consequence:
			if(if_execute):
				value = DB.get_value_consequence(consequence)
				DB.toggle_element(DB_requests,value,"TRUE")
				print("Requête débloquée : ",value)
			
		elif "LockRequest()" in consequence:
			if(if_execute):
				value = DB.get_id(current_request)
				DB.toggle_element(DB_requests,value,"FALSE")
				print("Requête courante verrouillée : ",value)
			
		elif "LockRequest" in consequence:
			if(if_execute):
				value = DB.get_value_consequence(consequence)
				DB.toggle_element(DB_requests,value,"FALSE")
				print("Requête verrouillée : ",value)
			
			
		#PERSONNAGES
		elif "UnlockCharacter" in consequence:
			if(if_execute):
				value = DB.get_value_consequence(consequence)
				DB.toggle_element(DB_characters,value,"TRUE")
				print("Personnage débloqué : ",value)
		
		elif "LockCharacterRandom()" in consequence :
			if(if_execute):
				
#				DB.update_available_characters(DB_characters,list_available_character_ID)
#
#				value = randi()%list_available_character_ID.size()
#				print(value,"list_available_character_ID",list_available_character_ID)
#				DB.toggle_element(DB_characters,value,"FALSE")
#				print("Personnage verrouillé : ",value)
				
				print("Un personnage aurait du disparaitre ...")
				
#				if(int(value[1]) >= randi()%100 ):
#					DB.toggle_element(DB_characters,value[0],"FALSE")
#					print("Personnage verrouillé : ",value[0])
#				else:
#					print("Personnage intact : ",value[0])

		
		elif "LockCharacter()" in consequence :
			if(if_execute):
				value = DB.get_character_id(current_request)
				DB.toggle_element(DB_characters,value,"FALSE")
				print("Personnage actuel verrouillé : ",value)
		
		elif "LockCharacter" in consequence :
			if(if_execute):
				value = DB.get_value_consequence(consequence)
				DB.toggle_element(DB_characters,value,"FALSE")
				print("Personnage verrouillé : ",value)
		
		
		
		elif "GameOver" in consequence :
			if(if_execute):
				#value = DB.get_value_consequence(consequence)
				#DB.toggle_element(DB_characters,value,"FALSE")
				GlobalScript.gameOverText="La ville est detruite"
				game_over()
		
		else:
			return consequence
		

	return null

func prepare_character_sprite():
	
	if(characters_today==null):
		game_over()
		
	
	character_tmp = characters_today.front()
	#request_tmp = requests_today.front()
	
	#cas spécial
#	if(DB.get_id(character_tmp)=="2"):
#		print("victime")
	
	
	character_image = DB.get_image( character_tmp )
	CharacterNode.set_texture(character_image)
	pass

func prepare_character_dialogue():
	request_tmp = requests_today.front()
	character_tmp = characters_today.front()
	
	var raw_text = DB.get_request_text(request_tmp)
	
	var text_split
	var text_split_head
	var text_split_tail
	var text
	#print(raw_text)
	
	var color = DB.get_color(character_tmp)
	#7AE4FF
	
	#request_consequence.split(";")
	if "[" in raw_text:
		text_split = raw_text.split("[")
		text_split_head = text_split[0]
		
		text_split = (text_split[1]).split("]")
		text_split_tail = text_split[0]
		
		#print("HEAD : ",text_split_head)
		#print("TAIL : ",text_split_tail)
		
		text = "[color=#" + color + "]" + DB.get_name(character_tmp) + " :\n\n[/color]" + text_split_head 
		text += "\n\n[color=#8B8378][i]" + text_split_tail + "[/i][/color]"
		
	else:
		text = "[color=#" + color + "]" + DB.get_name(character_tmp) + " :\n\n[/color]" + raw_text
		
	
	#var text = "[color=#7AE4FF]" + DB.get_name(character_tmp) + " :\n\n[/color]" + raw_text
	
	DialogueNode.set_bbcode(text)
	pass


func prepare_request_choices():
	
	request_tmp = requests_today.front() 
	var option_count = DB.get_answerCount(request_tmp)
	
	# Si il faut toucher à rien car on charge une sauvegarde
	if (is_loading_data):
		is_loading_data = false
		
	else:
		
		#Variables
		option_list_order.clear()
		var list_tmp = []
		var tmp
		
		#Remplisage de la liste
		for i in range(1,option_count+1):
			list_tmp.push_front(i)
		
		#print("before_1 :",list_tmp)
		#randomize()
		
		
		#Placement aléatoire des éléments
		for i in range(1,option_count+1):
			tmp = randi()%list_tmp.size()
			option_list_order.push_front(list_tmp[tmp])
			list_tmp.erase(list_tmp[tmp])
		
		#print("after :",option_list_order)
	
	
	var request_text
	#Première requête
	request_text = DB.get_answer(request_tmp,option_list_order[0])
	ChoiceOneNodeText.clear()
	ChoiceOneNodeText.append_bbcode(get_center_text_bbcode(request_text))
	
	#Seconde requête
	if option_count >= 2:
		request_text = DB.get_answer(request_tmp,option_list_order[1])
		ChoiceTwoNode.show()
		ChoiceTwoNodeText.clear()
		ChoiceTwoNodeText.append_bbcode(get_center_text_bbcode(request_text))
	else:
		ChoiceTwoNode.hide()
	
	#Troisième requête
	if option_count >= 3:
		request_text = DB.get_answer(request_tmp,option_list_order[2])
		ChoiceThreeNode.show()
		ChoiceThreeNodeText.clear()
		ChoiceThreeNodeText.append_bbcode(get_center_text_bbcode(request_text))
	else:
		ChoiceThreeNode.hide()
	
	#Quatrième requête
	if option_count >= 4:
		request_text = DB.get_answer(request_tmp,option_list_order[3])
		ChoiceFourNode.show()
		ChoiceFourNodeText.clear()
		ChoiceFourNodeText.append_bbcode(get_center_text_bbcode(request_text))
	else:
		ChoiceFourNode.hide()
	

	
	pass
	
func get_center_text_bbcode(text):
	
	return "[center]" + text + "[/center]"

func exit_game_when_false(boolean):
	if(boolean==false):
		get_tree().quit()
	pass

func _getAlignementBar():
	var AlignementBarNode = get_node("GUI/GUI_list/UpperUI/ZoneInformation/AlignementBar")
	return AlignementBarNode.get_node("AlignementTextureProgress")

func _getPopularityBar():
	var PopularityBarNode = get_node("GUI/GUI_list/UpperUI/ZoneInformation/PopularityBar")
	return PopularityBarNode.get_node("PanelContainer/HBoxContainer/PopularityTextureProgress")

func _getGoldCounter():
	var GoldCounterNode = get_node("GUI/GUI_list/UpperUI/ZoneInformation/HBoxContainer/GoldCounter")
	return GoldCounterNode.get_node("MarginContainer/HBoxContainer/Value")




func load_game_data():
	
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
	    return # Error! We don't have a save to load.
		
	save_game.open("user://savegame.save", File.READ)
	
	var load_dict = {}
	load_dict = parse_json(save_game.get_as_text())
	save_game.close()
	
	
	DB_characters = load_dict["DB_characters"]
	DB_requests = load_dict["DB_requests"]
	characters_today = load_dict["characters_today"]
	requests_today = load_dict["requests_today"]
	option_list_order = load_dict["option_list_order"]
	
	GoldCounter.set_text( load_dict["Gold"] )
	PopularityBar.set_value( load_dict["Popularity"] )
	AlignementBar.set_value( load_dict["Alignement"] )
	AudioStreamNode.seek(load_dict["playback_position"])
	
	DayCounterNode.text = "Jour : " + str(load_dict["day_count"])
	
	day_count = load_dict["day_count"]
	first_turn = load_dict["first_turn"]
	
	is_loading_data = true
	
	pass

func save():
	
	var save_dict = {
		"DB_characters" : DB_characters,
		"DB_requests" : DB_requests,
		"Popularity" : PopularityBar.get_value(),
		"Alignement" : AlignementBar.get_value(),
		"Gold" : GoldCounter.get_text(),
		"characters_today" : characters_today ,
		"requests_today" : requests_today,
		"option_list_order" : option_list_order,
		"playback_position" : AudioStreamNode.get_playback_position(),
		"day_count" : day_count,
		"first_turn" : first_turn
	}
	
	return save_dict

func _on_Save_pressed():
	
	print("début sauvegarde")
	
	var save_game = File.new()
	#C:\Users\XXXXX\AppData\Roaming\Godot\app_userdata\Mage de marque
	save_game.open("user://savegame.save", File.WRITE)
	
	print( OS.get_user_data_dir()  )
	
	
	var save_nodes = get_tree().get_nodes_in_group("Persist")
	for i in save_nodes:
		var node_data = i.call("save");
		save_game.store_line(to_json(node_data))
	save_game.close()
	
	print("fin sauvegarde")
	
	pass # replace with function body
