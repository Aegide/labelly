extends TextureButton

func _on_PauseButton_pressed():
	get_node("/root/Game/GUI/GUI_list/UpperUI/Node/Wood_counter").set_visible(false)
	get_node("/root/Game/GUI/PauseZone").set_visible(true)
	
	pass 

func _on_Return_pressed():
	get_node("/root/Game/GUI/GUI_list/UpperUI/Node/Wood_counter").set_visible(true)
	get_node("/root/Game/GUI/PauseZone").set_visible(false)
	pass

func _on_MainMenu_pressed():
	get_tree().change_scene("res://MainMenu.tscn")
	pass # replace with function body
