extends MarginContainer



func _ready():

	var gameOverText = get_node("MarginContainer/VBoxContainer/TextGameOver")
	gameOverText.text = GlobalScript.gameOverText

	var continueButton = get_node("MarginContainer/VBoxContainer/VBoxContainer/Continue")

	var file2Check = File.new()
	var doFileExists = file2Check.file_exists("user://savegame.save")

	if(doFileExists):
		continueButton.disabled=false
	else:
		continueButton.disabled=true
	pass


func _on_NewGame_pressed():
	GlobalScript.isNewGame=true
	get_tree().change_scene("res://Game.tscn")
	pass # replace with function body


func _on_Continue_pressed():
	GlobalScript.isNewGame=false
	get_tree().change_scene("res://Game.tscn")
	pass # replace with function body
